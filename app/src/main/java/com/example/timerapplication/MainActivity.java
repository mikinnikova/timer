package com.example.timerapplication;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    //    @BindView(R.id.start)
//    Button start;
//
//    @BindView(R.id.seconds)
//    EditText seconds;
    EditText seconds;
    Button start;
    int sec;


    Handler handler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == 1) {
                sec = Integer.parseInt(seconds.getText().toString());
                sec--;
                seconds.setText(String.valueOf(sec));
                handler.sendEmptyMessageDelayed(1, 1000);
                if (sec == 0) {
                    Toast.makeText(MainActivity.this, R.string.time_is_out, Toast.LENGTH_SHORT).show();
                    handler.removeMessages(1);

                    seconds.setText("");
                    start.setEnabled(true);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seconds = (EditText) findViewById(R.id.seconds);
        start = (Button) findViewById(R.id.start);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start.setEnabled(false);
                Message msg = new Message();
                msg.what = 1;
                handler.sendMessage(msg);

            }
        });

    }


//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        handler.sendEmptyMessage(1);
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        handler.removeMessages(1);
//
//
//    }
}


